package com.cerezaconsulting.cocosapp.data.entities;

import java.io.Serializable;

/**
 * Created by manu on 04/08/16.
 */
public class AccessTokenEntity implements Serializable {
    private String token;
    private UserEntity user;
    private String tokenEntryFood;

    private String tokenFood;

    private String tokenDessert;

    private String tokenDrink;

    public String getTokenEntryFood() {
        return tokenEntryFood;
    }

    public void setTokenEntryFood(String tokenEntryFood) {
        this.tokenEntryFood = tokenEntryFood;
    }

    public String getTokenFood() {
        return tokenFood;
    }

    public void setTokenFood(String tokenFood) {
        this.tokenFood = tokenFood;
    }

    public String getTokenDessert() {
        return tokenDessert;
    }

    public void setTokenDessert(String tokenDessert) {
        this.tokenDessert = tokenDessert;
    }

    public String getTokenDrink() {
        return tokenDrink;
    }

    public void setTokenDrink(String tokenDrink) {
        this.tokenDrink = tokenDrink;
    }

    public String getAccessToken() {
        return token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }
}
