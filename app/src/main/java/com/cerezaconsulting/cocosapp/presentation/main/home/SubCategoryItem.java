package com.cerezaconsulting.cocosapp.presentation.main.home;


import com.cerezaconsulting.cocosapp.data.entities.FoodsEntity;
import com.cerezaconsulting.cocosapp.data.entities.SubCatEntity;

/**
 * Created by katherine on 24/04/17.
 */

public interface SubCategoryItem {

    void clickItem(FoodsEntity foodsEntity);

    void deleteItem(FoodsEntity foodsEntity, int position);
}
