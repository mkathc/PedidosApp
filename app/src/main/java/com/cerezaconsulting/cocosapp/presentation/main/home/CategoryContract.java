package com.cerezaconsulting.cocosapp.presentation.main.home;

import com.cerezaconsulting.cocosapp.core.BasePresenter;
import com.cerezaconsulting.cocosapp.core.BaseView;
import com.cerezaconsulting.cocosapp.data.entities.BusServicesEntity;
import com.cerezaconsulting.cocosapp.data.entities.FoodsEntity;
import com.cerezaconsulting.cocosapp.data.entities.SubCatEntity;

import java.util.ArrayList;

/**
 * Created by katherine on 12/05/17.
 */

public interface CategoryContract {
    interface View extends BaseView<Presenter> {

        void getListCategories(ArrayList<FoodsEntity> list);

        void clickItemCategories(FoodsEntity foodsEntity);

        void sendDialogConfirm(boolean confirm);

        void sendPedidoToBusServiceResponse();

        boolean isActive();
    }

    interface Presenter extends BasePresenter {


        void loadOrdersFromPage(int page);

        void loadfromNextPage(int id);

        void startLoad(int id);

        void getCategories(int page);

        void sendFoodToBusServices(BusServicesEntity busServicesEntity);

        void sendEntryfoodToBusServices(BusServicesEntity busServicesEntity);

        void sendDessertToBusServices(BusServicesEntity busServicesEntity);

        void sendDrinkToBusServices(BusServicesEntity busServicesEntity);


    }
}
